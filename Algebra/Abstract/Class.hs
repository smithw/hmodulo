{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-}

module Algebra.Abstract.Class
	( Group (..)
	, Ring (..)
	, RootRing (..)
	, Field (..)
	, Module (..)
	, VectorSpace (..)
	, InnerModule (..)
	, NormedModule (..)
	, NormedEq (..)
	) where

import Data.Monoid
import Control.Monad
import Control.Monad.Reader

class (Monoid a) => Group a where
	zero :: a
	(^+), (^-) :: a -> a -> a
	gnegate :: a -> a

	zero = mempty
	(^+) = mappend
	x ^- y = x ^+ gnegate y
	gnegate = (zero ^-)

class (Group a) => Ring a where
	(^*) :: a -> a -> a
	one :: a

class (Ring a) => RootRing a where
	rsqrt :: a -> a

class (Ring a) => Field a where
	(^/) :: a -> a -> a
	frecip :: a -> a

	frecip = (one ^/)
	x ^/ y = x ^* frecip y

class (Group a, Ring k) => Module a k | a -> k where
	(^**) :: k -> a -> a

class (Module a k, Field k) => VectorSpace a k | a -> k where
	(^//) :: a -> k -> a

	x ^// y = frecip y ^** x

class (Module a k, Ring k) => InnerModule a k | a -> k where
	(.*) :: a -> a -> k

class (InnerModule a k, RootRing k) => NormedModule a k | a -> k where
	norm :: a -> k
	(<->) :: a -> a -> k

	norm = rsqrt . join (.*) 
	(<->) = (norm .) . (^-)

class (NormedModule a k, Ord k) => NormedEq a k | a -> k where
	normedErr :: a -> k
	combineErr :: a -> a -> k
	(^==) :: a -> a -> Bool

	normedErr = const mempty
	combineErr x y = normedErr x <> normedErr y

	x ^== y = (x <-> y) < combineErr x y


