{-# LANGUAGE DataKinds, GADTs, FlexibleInstances, MultiParamTypeClasses #-}

module Algebra.Abstract.Matrix
	( Matrix, SquareMatrix
	, mat, mvmap, idMatrix, fromLists, toLists, row, column
	, transpose, (^***), (<***>), matrixZipWith, multApply
	, coords
	) where

import qualified Data.Foldable as F
import qualified Data.Traversable as T
import Control.Applicative
import Data.Monoid

import Algebra.Abstract.Class
import Algebra.Abstract.Vector hiding (zipWith)
import qualified Algebra.Abstract.Vector as Vector (zipWith)

newtype Matrix n m a = Matrix { getVector :: Vector n (Vector m a) }
type SquareMatrix n a = Matrix n n a

mvmap :: (Vector n (Vector m a) -> Vector k (Vector j b)) -> Matrix n m a -> Matrix k j b
mvmap = (Matrix .) . (. getVector)

mat :: (Natural m, Natural n) => a -> Matrix n m a
mat = Matrix . vec . vec

row :: Vector n a -> Matrix T1 n a
row = Matrix . vec

column :: Vector n a ->  Matrix n T1 a
column = Matrix . fmap (:^ VZero)

fromLists :: (Natural m, Natural n, Monoid a) => [[a]] -> Matrix n m a
fromLists = Matrix . fromList . map fromList

toLists :: (Natural m, Natural n) => Matrix n m a -> [[a]]
toLists = map F.toList . F.toList . getVector

idMatrix :: (Natural n, Ring a) => SquareMatrix n a
idMatrix = fromLists $ iterate (zero:) [one]

coords :: (Natural n, Natural m, Ring a) => Matrix n m (a, a)
coords = fromLists [iterate ((^+ one) <$>) (i, one) | i <- iterate (^+ one) one]

transpose :: (Natural m) => Matrix n m a -> Matrix m n a
transpose = mvmap transpose'
	where
		transpose' :: (Natural m) => Vector n (Vector m a) -> Vector m (Vector n a)
		transpose' VZero = vec VZero
		transpose' (x :^ xs) = Vector.zipWith (:^) x $ transpose' xs

matrixZipWith :: (a -> b -> c) -> Matrix n m a -> Matrix n m b -> Matrix n m c
matrixZipWith f (Matrix v1) (Matrix v2) = Matrix $ Vector.zipWith (Vector.zipWith f) v1 v2

multApply :: (Natural m, Natural n, Natural k, Monoid c) => 
	(a -> b -> c) -> Matrix n m a -> Matrix m k b -> Matrix n k c
multApply f (Matrix vx) y = Matrix $ fmap (flip fmap vy' . f') vx
	where
		Matrix vy' = transpose y
		f' = applyFold2 (<>) (pure f) mempty

(^***) :: (Natural m, Natural n, Natural k, Ring a) =>
	Matrix n m a -> Matrix m k a -> Matrix n k a
(^***) = multApply (^*)

(<***>) :: (Natural m, Natural n, Natural k, Monoid c) =>
	Matrix n m (b -> c) -> Matrix m k b -> Matrix n k c
(<***>) = multApply ($)

instance Functor (Matrix m n) where
	fmap = T.fmapDefault

instance F.Foldable (Matrix m n) where
	foldMap = T.foldMapDefault

instance T.Traversable (Matrix m n) where
	-- traverse :: Applicative f => (a -> f b) -> Matrix m n a -> f (Matrix m n b)
	traverse f m = Matrix <$> (T.traverse (T.traverse f) . getVector $ m)

instance (Show a, Natural n, Natural m) => Show (Matrix m n a) where
	show m = "fromLists " ++ (show . toLists $ m)

instance (Monoid a, Natural n, Natural m) => Monoid (Matrix n m a) where
	mempty = mat mempty
	mappend = matrixZipWith (<>)

instance (Group a, Natural n, Natural m) => Group (Matrix n m a) where
	gnegate = fmap gnegate

instance (Ring k, Natural n, Natural m) => Module (Matrix n m k) k where
	h ^** m = fmap (^* h) m

instance (Field k, Natural n, Natural m) => VectorSpace (Matrix n m k) k

instance (Ring k, Natural n) => Ring (SquareMatrix n k) where
	(^*) = (^***)
	one = idMatrix
