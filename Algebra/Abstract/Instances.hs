{-# LANGUAGE MultiParamTypeClasses #-}

module Algebra.Abstract.Instances
	(
	) where

import Algebra.Abstract.Class
import Data.Monoid

instance Monoid Integer where
	mempty = 0
	mappend = (+)

instance Group Integer where
	gnegate = negate

instance Ring Integer where
	(^*) = (*)
	one = 1

instance Module Integer Integer where
	(^**) = (*)

instance Monoid Int where
	mempty = 0
	mappend = (+)

instance Group Int where
	gnegate = negate

instance Ring Int where
	(^*) = (*)
	one = 1

instance Module Int Int where
	(^**) = (*)

instance Monoid Double where
	mempty = 0
	mappend = (+)

instance Group Double where
	gnegate = negate

instance Ring Double where
	(^*) = (*)
	one = 1

instance RootRing Double where
	rsqrt = sqrt

instance Field Double where
	(^/) = (/)
	frecip = recip

instance Module Double Double where
	(^**) = (*)

instance VectorSpace Double Double

instance InnerModule Double Double where
	(.*) = (*)

instance NormedModule Double Double where
	norm = abs
