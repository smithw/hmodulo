{-# LANGUAGE DataKinds, GADTs, FlexibleInstances, MultiParamTypeClasses #-}

module Algebra.Abstract.Vector
	( Vector (..)
	, Algebra.Abstract.Vector.zipWith, vec, fromList, applyFold, applyFold2
	, TNatural (..)
	, T1, T2, T3, T4
	, Natural, NaturalGenerator
	) where

import qualified Data.Foldable as F
import qualified Data.Traversable as T
import Data.Monoid
import Control.Applicative

import Algebra.Abstract.Class

data TNatural = T0 | TSucc TNatural

type T1 = TSucc T0
type T2 = TSucc T1
type T3 = TSucc T2
type T4 = TSucc T3

class Natural n where
	choice :: (NaturalGenerator g) => g n

class NaturalGenerator g where
	choiceZero :: g T0
	choiceSucc :: (Natural n) => g (TSucc n)

instance Natural T0 where
	choice = choiceZero

instance (Natural n) => Natural (TSucc n) where
	choice = choiceSucc

data Vector n k where
	VZero :: Vector T0 k
	(:^) :: k -> Vector n k -> Vector (TSucc n) k

infixr 3 :^

instance Functor (Vector n) where
	fmap = T.fmapDefault

instance F.Foldable (Vector n) where
	foldMap = T.foldMapDefault

instance T.Traversable (Vector n) where
	-- traverse :: Applicative m => (a -> m b) -> Vec n a -> m (Vec n b)
	traverse _ VZero = pure VZero
	traverse f (x :^ xs) = (:^) <$> f x <*> T.traverse f xs

zipWith :: (a -> b -> c) -> Vector n a -> Vector n b -> Vector n c
zipWith _ VZero VZero = VZero
zipWith f (x :^ xs) (y :^ ys) = f x y :^ Algebra.Abstract.Vector.zipWith f xs ys

newtype MkVec a n = MkVec { runMkVec :: a -> Vector n a }

vec :: (Natural n) => a -> Vector n a
vec = runMkVec choice

instance NaturalGenerator (MkVec a) where
	choiceZero = MkVec $ const VZero
	choiceSucc = MkVec $ \x -> x :^ vec x

instance (Natural n) => Applicative (Vector n) where
	pure = vec
	(<*>) = Algebra.Abstract.Vector.zipWith ($)

applyFold :: (Applicative f, F.Foldable f) =>
	(b -> c -> c) -> f (a -> b) -> c -> f a -> c
applyFold f1 f2 z fa = F.foldr f1 z $ f2 <*> fa

applyFold2 :: (Applicative f, F.Foldable f) =>
	(c -> d -> d) -> f (a -> b -> c) -> d -> f a -> f b -> d
applyFold2 f1 f2 z fa fb = F.foldr f1 z $ f2 <*> fa <*> fb

instance (Monoid a, Natural n) => Monoid (Vector n a) where
	mempty = vec mempty
	mappend = Algebra.Abstract.Vector.zipWith (<>)

instance (Group a, Natural n) => Group (Vector n a) where
	gnegate = fmap gnegate

instance (Ring k, Natural n) => Module (Vector n k) k where
	h ^** v = fmap (h ^*) v

instance (Field k, Natural n) => VectorSpace (Vector n k) k

instance (Ring k, Natural n) => InnerModule (Vector n k) k where
	x .* y = applyFold2 (<>) (pure (^*)) mempty x y --F.foldr (^+) zero $ (^*) <$> x <*> y
	
instance (RootRing k, Natural n) => NormedModule (Vector n k) k

instance (Show a) => Show (Vector n a) where
	show x = "fromList " ++ (show . F.toList) x

newtype MkFromList a n = MkFromList { runFromList :: [a] -> Vector n a }

fromList :: (Natural n, Monoid a) => [a] -> Vector n a
fromList = runFromList choice

instance (Monoid a) => NaturalGenerator (MkFromList a) where
	choiceZero = MkFromList . const $ VZero
	choiceSucc = MkFromList $ \xs -> case xs of
		x : xs' -> x :^ fromList xs'
		[] -> vec mempty
