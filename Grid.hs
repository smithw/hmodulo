module Grid where

-- import qualified Piece as P
import Common
import Data.Char

data Grid = Grid {
	getGrid :: [Int],
	getDepth :: Int,
	getWidth :: Int,
	getHeight :: Int
	} deriving (Show)

mkGrid :: String -> Grid
mkGrid str = let (g, d, w, _, h) = foldr f ([], 0, 0, 0, 0) str in Grid g (d+1) w h
	where
		f char ([], d, w, c, h) = let n = digitToInt char
			in ([n], n, 1, 1, 1)
		f ',' (g, d, w, c, h) = (g, d, w, 1, h + 1)
		f char (g, d, w, c, h) = let n = digitToInt char
			in (n : g, max n d, max w c, c + 1, h)

plot :: Grid -> String
plot (Grid { getGrid=g, getWidth=w })= snd . foldr f (1, "") $ g
	where
		f block (pos, str) = if pos <= w
			then (pos + 1, intToDigit block : str)
			else (2, intToDigit block : '\n' : str)
