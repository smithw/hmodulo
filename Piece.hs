module Piece where

import Control.Monad
import Common
import qualified Grid as G

type Mask = [Bool]

instance Show Piece where
	show = (++"\"") . ("mkPiece \""++) . drop 1 . join . foldr f [] . getUnbound
		where
			f = (:) . (',':) . foldr g ""
			g block str = if block then '#' : str else '.' : str


data Piece = Piece { getUnbound :: [Mask], getMasks :: Maybe [Mask]} deriving (Show)

-- instance Show Piece where
-- 	show = (++"\"") . ("mkPiece \""++) . drop 1 . join . foldr f [] . getUnbound
-- 		where
-- 			f mask lines = (',' : foldr g "" mask) : lines
-- 			g block str = case block of
-- 				True -> '#' : str
-- 				False -> '.' : str

plot :: Int -> Mask -> String
plot w = snd . foldr f (1, "")
	where
		f block (pos, str) = if pos <= w
			then (pos + 1, mkBlock block : str)
			else (2, mkBlock block : '\n' : str)
		mkBlock b = if b then '#' else '.'

getWidth :: Piece -> Int
getWidth = foldr (max . length) 0 . getUnbound

getHeight :: Piece -> Int
getHeight = length . getUnbound

normalize :: Piece -> Piece
normalize piece@(Piece unbound bound) = flip Piece bound $ map (pad False w) unbound
	where w = getWidth piece

mkPiece :: String -> Piece
mkPiece = normalize . flip Piece Nothing . foldr f []
	where
		f block masks = case block of
			',' -> [] : masks
			'.' -> mkBlock False masks
			_ -> mkBlock True masks
		mkBlock used [] = [[used]]
		mkBlock used (x:xs) = (used : x) : xs

bindPiece :: G.Grid -> Piece -> Piece
bindPiece (G.Grid { G.getWidth=w, G.getHeight=h }) piece@(Piece unbound _) = Piece unbound $ Just bound
	where
		bound = map join . join . foldr g [] $ [0..(h - getHeight piece)]
		padline = replicate w False
		line = foldr f [] [0..(w - getWidth piece)]
		f _ [] = [map (pad False w) unbound]
		f _ xs@(x:_) = map (pushList False) x : xs
		g _ [] = [map (pad padline h) line]
		g _ xs@(x:_) = map (pushList padline) x : xs

